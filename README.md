A git commit process which will be good to adopt. By using below steps we will not face any merging big issues and work going to smooth.

When ever we are going to start work we will follow below steps. 

1. Open Xcode/ Source tree
2. Switch to 'develop' branch 
3. Pull 'develop' branch to fetch latest code.
4. Create a new feature branch from develop and named will look like [ developerNameAbbreviation-featureToWork e.g. rg-menu-updates, rg-bug-fixing (rg for Russel Gafoor) ]. Each developer will work on own its own feature branch. 
5. When you complete his work in the feature branch then do following steps.

Commit and push your feature branch work. 
Switch to 'develop'
Pull latest 'develop' code
Merge 'develop' into your feature branch
Resolved conflicts if any
Commit and Push your feature branch.

Now go to the Bitbucket in web and open 'Pull Requests' section.

Select 'Create a pull request'
Select source branch in left side which should be your feature branch e.g. 'rg-menu-updates' 
Select destination branch in right side which should be 'develop' branch. 
It would be good to close your branch after merging so checked 'Close branch after the pull request is merged' at bottom.
Click on 'Create pull request'
Then Click on 'Merge' to merge your code. 

It would be good practise to create a new feature branch every time from develop if you are even to write a single line. 

NOTE: No one can directly write code in the 'develop' branch.

If you was start working mistakenly in the develop branch then no issue - Just create a new branch when you realize - Your work will automatically shifted to your feature branch. 
